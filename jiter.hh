#ifndef _JITER_HH_
#define _JITER_HH_

template <typename T> class jiter;

template <typename T, int N>
class jiter<T[N]>
{
protected:
  int i;
  T (&a)[N];
public:
  jiter(T (&_a)[N]) : i(0), a(_a) {}
  operator bool() { return i<N; }
  T& operator*() { return a[i]; }
  T* operator->() { return &a[i]; }
  void operator++() { ++i; }
};

template <typename T>
class jiter<T*>
{
protected:
  int i;
  int n;
  T* a;
public:
  jiter(T* _a, int _n)
    : i(0), n(_n), a(_a) {}
  operator bool() { return i<n; }
  T& operator*() { return a[i]; }
  T* operator->() { return &a[i]; }
  void operator++() { ++i; }
};

template <>
class jiter<int>
{
protected:
  int i;
  int n;
public:
  jiter(int lo, int hi) : i(lo), n(hi) {}
  jiter(int hi) : i(0), n(hi) {}
  operator bool() { return i<n; }
  int operator*() { return i; }
  void operator++() { ++i; }
};

template <typename C>
class jiter
{
protected:
  typename C::iterator _beg;
  typename C::iterator _end;
public:
  jiter(C& c) : _beg(c.begin()), _end(c.end()) {}
  operator bool() { return _beg != _end; }
  typename C::value_type& operator*() { return *_beg; }
  typename C::value_type* operator->(){ return _beg.operator ->(); }
  void operator++() { ++_beg; }
};

template <typename C>
class jiter<const C>
{
protected:
  typename C::const_iterator _beg;
  typename C::const_iterator _end;
public:
  jiter(const C& c) : _beg(c.begin()), _end(c.end()) {}
  operator bool() { return _beg != _end; }
  const typename C::value_type& operator*() { return *_beg; }
  const typename C::value_type* operator->(){ return _beg.operator ->(); }
  void operator++() { ++_beg; }
};

template <typename C>
class jiterkeys
{
protected:
  jiter<C> _it;
public:
  jiterkeys(C& c) : _it(jiter<C>(c)) {}
  operator bool() { return _it; }
  typename C::value_type::first_type& operator*() { return _it->first; }
  typename C::value_type::first_type* operator->() { return &_it->first; }
  void operator++() { ++_it; }
};

template <typename C>
class jiterkeys<const C>
{
protected:
  jiter<const C> _it;
public:
  jiterkeys(const C& c) : _it(jiter<const C>(c)) {}
  operator bool() { return _it; }
  const typename C::value_type::first_type& operator*() { return _it->first; }
  const typename C::value_type::first_type* operator->() { return &_it->first; }
  void operator++() { ++_it; }
};

template <typename C>
class jitervalues
{
protected:
  jiter<C> _it;
public:
  jitervalues(C& c) : _it(jiter<C>(c)) {}
  operator bool() { return _it; }
  typename C::value_type::second_type& operator*() { return _it->second; }
  typename C::value_type::second_type* operator->() { return &_it->second; }
  void operator++() { ++_it; }
};

template <typename C>
class jitervalues<const C>
{
protected:
  jiter<const C> _it;
public:
  jitervalues(const C& c) : _it(jiter<const C>(c)) {}
  operator bool() { return _it; }
  const typename C::value_type::second_type& operator*() { return _it->second; }
  const typename C::value_type::second_type* operator->() { return &_it->second; }
  void operator++() { ++_it; }
};

#endif
