#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsLineItem>
#include <list>
#include <QResizeEvent>
#include "rtview.hh"
#include <QPen>
#include <QBrush>
#include <QColor>
#include <QPainter>
#include <functional>
#include <cmath>
#include <QPainterPath>
#include <QGraphicsEllipseItem>
#include <QGraphicsTextItem>
#include <QFont>

#include <iostream>

using namespace std;

static const qreal TOO_CLOSE = 0.1;
static const qreal OFFSET = 0.2;

qreal distance(RTLine& line, RTNode& point)
{
  qreal a = ((qreal)(line.r1-line.r2))/((qreal)(line.c1-line.c2));
  qreal b = ((qreal)line.r1) - a*((qreal) line.c1);
  qreal x = (((qreal)point.r)-b)/a;
  qreal y = a*((qreal)point.c) + b;
  qreal dx = x-((qreal)point.c);
  qreal dy = y-((qreal)point.r);
  if (dx<0) dx = -dx;
  if (dy<0) dy = -dy;
  return (dx==0.0)?0.0:((dy==0.0)?0.0:(dx*dy)/sqrt(dx*dx+dy*dy));
}

RTView::RTView(RTDesc& desc, RTView** first)
  : QGraphicsView(), NCOLS(desc.NCOLS), NROWS(desc.NROWS)
{
  next = *first;
  if (next) next->prev = &next;
  prev = first;
  *first = this;

  setAttribute(Qt::WA_DeleteOnClose);

  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setViewportUpdateMode(FullViewportUpdate);
  setRenderHints(QPainter::Antialiasing);
  resize(400,400);

  scene = new QGraphicsScene();

  QColor gray(qRgb(200,200,200));
  QPen pen(gray);
  pen.setStyle(Qt::DashLine);

  qreal penw = 0.3;
  pen.setWidthF(penw);

  for (int r=0; r<NROWS; ++r)
    scene->addLine(0, -r*SEP, (NCOLS-1)*SEP, -r*SEP, pen)->setZValue(0.0);
  for (int c=0; c<NCOLS; ++c)
    scene->addLine(c*SEP, 0, c*SEP, -(NROWS-1)*SEP, pen)->setZValue(0.0);

  QPen on(Qt::red);
  QPen off(Qt::blue);

  on.setWidthF(penw);
  off.setWidthF(penw);

  {
    list<RTLine>::iterator
      beg(desc.succs.begin()),
      end(desc.succs.end  ());

    while (beg != end)
      {
	// if the segment is between nodes in adjacent columns
	// or adjacent lines, then just draw a line since it cannot
	// cross an intervening node (because there is none).
	int dc = beg->c2 - beg->c1;
	int dr = beg->r2 - beg->r1;
	if (dc<0) dc = -dc;
	if (dr<0) dr = -dr;
	if (dc==1 || dr==1)
	  {
	    scene->addLine(beg->c1*SEP, -beg->r1*SEP,
			   beg->c2*SEP, -beg->r2*SEP,
			   (beg->det?on:off))->setZValue(1.0);
	    ++beg;
	    continue;
	  }

	bool is_vertical = (beg->c1 == beg->c2);

	// for each visible node that is not an end-point
	// check if the segment is too close to it: in that
	// it must be drawn with a curve, i.e. it must bend!
	bool must_bend = false;
	{
	  list<RTNode>::iterator
	    nbeg(desc.nodes.begin()),
	    nend(desc.nodes.end  ());

	  while (nbeg != nend)
	    {
	      // if node should not be displayed
	      if (nbeg->status == -1)
		{
		  ++nbeg;
		  continue;
		}
	      // check if the point is an end-point of the segment
	      if (((nbeg->c == beg->c1) && (nbeg->r == beg->r1)) ||
		  ((nbeg->c == beg->c2) && (nbeg->r == beg->r2)))
		{
		  ++nbeg;
		  continue;
		}

	      // if the segment is vertical
	      if (is_vertical)
		{
		  // check if the point is outside the segment's bounding slice
		  if ((nbeg->r < beg->r1 && nbeg->r < beg->r2) ||
		      (nbeg->r > beg->r1 && nbeg->r > beg->r2))
		    {
		      ++nbeg;
		      continue;
		    }
		  qreal dist = nbeg->c - beg->c1;
		  if (dist < 0.0) dist = -dist;
		  // is the point too close?
		  if (dist <= TOO_CLOSE)
		    {
		      must_bend = true;
		      break;
		    }
		  ++nbeg;
		  continue;
		}
	      // the segment is not vertical
	      // check if the point is outside the segment's bounding box
	      if ((nbeg->c < beg->c1 && nbeg->c < beg->c2) ||
		  (nbeg->c > beg->c1 && nbeg->c > beg->c2) ||
		  (nbeg->r < beg->r1 && nbeg->r < beg->r2) ||
		  (nbeg->r > beg->r1 && nbeg->r > beg->r2))
		{
		  ++nbeg;
		  continue;
		}
	      // check if the point is too close
	      if (distance(*beg,*nbeg) <= TOO_CLOSE)
		{
		  must_bend=true;
		  break;
		}
	      ++nbeg;
	    }
	}

	// if it doesn't have to bend
	if (!must_bend)
	  {
	    // draw a straight line
	    scene->addLine(beg->c1*SEP, -beg->r1*SEP,
			   beg->c2*SEP, -beg->r2*SEP,
			   (beg->det?on:off))->setZValue(1.0);
	    ++beg;
	    continue;
	  }

	// else we must draw a bezier curve using a heuristic.
	// we choose a quadractic bezier curve where the control
	// point is essentially in the middle of the segment, but
	// offset in the up-right direction with a distance that
	// is proportional to the length of the segment.

	// midpoint
	qreal c0 = ((qreal)(beg->c1+beg->c2))/2.0;
	qreal r0 = ((qreal)(beg->r1+beg->r2))/2.0;
	qreal dx0 = ((qreal)(beg->c1-beg->c2));
	qreal dy0 = ((qreal)(beg->r1-beg->r2));
	qreal d0 = sqrt(dx0*dx0+dy0*dy0);
	qreal D = sqrt((NROWS-1)*(NROWS-1) + (NCOLS-1)*(NCOLS-1));
	// control point
	qreal C0 = c0 + d0*OFFSET/D;
	qreal R0 = r0 + d0*OFFSET/D;
	// add curve
	QPainterPath p0(QPointF(beg->c1*SEP,-beg->r1*SEP));
	p0.quadTo(C0*SEP,-R0*SEP,beg->c2*SEP,-beg->r2*SEP);
	scene->addPath(p0,(beg->det?on:off))->setZValue(1.0);

	++beg;
      }
  }

  // add nodes
  qreal radius = 0.5;
  QBrush onb(Qt::red);
  QBrush offb(Qt::blue);

  {
    std::list<RTNode>::iterator
      beg(desc.nodes.begin()),
      end(desc.nodes.end());

    while (beg != end)
      {
	if (beg->status >= 0)
	  {
	    QGraphicsEllipseItem* disk =
	      scene->addEllipse(beg->c*SEP-radius, -radius-beg->r*SEP,
				2.0*radius, 2.0*radius,
				(beg->status ? on : off),
				(beg->status ? onb : offb));
	    disk->setZValue(2.0);
	    if (beg->label.size() > 0)
	      {
		QFont font;
		font.setBold(true);
                font.setStretch(QFont::Expanded);
		QGraphicsTextItem* text =
		  scene->addText(beg->label.c_str(), font);
		text->setZValue(3.0);
                text->scale(0.2, 0.2);
		text->moveBy(beg->c*SEP, -radius-beg->r*SEP);
		QRectF bbox = text->sceneBoundingRect();
		text->moveBy(-bbox.width() / 2.0, -bbox.height() / 2.0);
		text->setDefaultTextColor(Qt::gray);
	      }
	  }
	++beg;
      }
  }

  setScene(scene);
}

void RTView::resizeEvent(QResizeEvent* event)
{
  QGraphicsView::resizeEvent(event);
  qreal scalew = event->size().width () / ((qreal) (NCOLS+1)*SEP);// ((qreal) scene->width ());
  qreal scaleh = event->size().height() / ((qreal) (NROWS+1)*SEP);//scene->height());
  qreal s = (scalew<scaleh)?scalew:scaleh;
  resetTransform();
  scale(s, s);
  qreal x0 =  ((qreal) (NCOLS*SEP)) / 2.0;
  qreal y0 = -((qreal) (NROWS*SEP)) / 2.0;
  centerOn(x0, y0);
}

void RTView::closeEvent(QCloseEvent* event)
{
  *prev = next;
  if (next) next->prev = prev;
  next = NULL;
  prev = NULL;
  QGraphicsView::closeEvent(event);
}

//==============================================================================
// RTInspector
//==============================================================================

RTInspector::RTInspector(const string& name)
  : first(NULL), _name(name) {}

void RTInspector::finalize()
{
  while (first) first->close();
}

RTInspector::~RTInspector()
{ finalize(); }

string RTInspector::name()
{ return _name; }


