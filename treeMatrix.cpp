#include "gecode/search.hh"
#include "config.hh"
#ifndef PG_NO_GIST
#include <digecode/gist.hh>
#include <gecode/gist.hh>
#endif
#include <iostream>
#include "Tree.hh"
#include "Grammar.hh"
#include "Lexicon.hh"
#ifndef PG_NO_GIST
#include "rtview.hh"
#endif

using namespace digecode;
using namespace std;

//==============================================================================
// main
//==============================================================================

int main(int argc, char** argv) {
  
  if (argc < 3)
    {
      cerr << "usage: " << argv[0] << " NROWS word*" << endl;
      return -1;
    }

  Lexicon lexicon;

  Grammar GRAM("none");
  GRAM << "S" << "NP" << "VP" << "V" << "N" << "D";
  //GRAM << "NP" << "N" << "D";
  //GRAM.req("NP", "D") << "N";
  //GRAM.con("N");
  //GRAM.con("D");

  GRAM.con("S") << "NP" << "VP"; //1
  GRAM.con("NP") << "N" << "D";
  GRAM.con("VP") << "V" << "NP";
  GRAM.obl("S","VP");
  GRAM.uni("S","VP");
  GRAM.obl("S","NP");
  GRAM.uni("S","NP");
  GRAM.lin("S", "NP", "VP");
  GRAM.obl("VP","V");
  GRAM.uni("VP", "V");
  GRAM.uni("VP","NP");
  GRAM.uni("NP", "D");
  GRAM.obl("NP","N");
  GRAM.req("NP", "D") << "N";
  GRAM.lin("NP", "D", "N");


  GRAM.con("V");

  //GRAM.req("VP", "NP") << "VP";
  ///GRAM.exc("VP","VP","V");


  GRAM.con("N");
  GRAM.con("D");
  //GRAM.adj("S","NP","VP");
  //GRAM.adj("NP","D","N");
  //GRAM.adj("VP","D","V");


  //lexicon["Pierre"] = GRAM["NP"];
  lexicon["Pierre"] = GRAM["N"];
  lexicon["mange"] = GRAM["V"];
  lexicon["la"] = GRAM["D"];
  lexicon["pomme"] = GRAM["N"];

  int n = atoi(argv[1]);
  int m = argc - 2;
  argv += 2;

  Tree* pb = new Tree(n,m,argv,GRAM,lexicon,false);
  //Tree* pb = new Tree(n,m,argv,GRAM,lexicon,true);

  Space* space = new Space(*pb);

#ifndef PG_NO_GIST

  Gist::Print<Space> printer("solutions", *pb);
  ViewGrid<Space> grid("Grid");
  Gecode::Gist::Options options;
  options.inspect.click(&printer);
  options.inspect.click(&grid);
  //Gist::dfs(space, options);
  Gecode::Gist::bab(space, options);
  delete space;

#else

  // creer un moteur de recherche sur le probleme
  // pour trouver une solution
  // DFS<Tree> engine(pb);
  // for optimisation
  Gecode::Search::Options options;
  // options.threads = 3.0;
  GecodeBAB<Space> engine(space, options);
  // plus besoin du probleme
  delete space;
  // imprimer les solutions
  int sol=0;
  while (Space* solution=engine.next()) {
    cout << "---------------" << endl;
    cout << "Solution " << ++sol << " : " << endl;
    solution->print(cout);
    delete solution;
  }
#endif

  return 0;
}

