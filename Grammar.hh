#ifndef _GRAMMAR_HH_
#define _GRAMMAR_HH_

#include "Property.hh"
#include <list>
#include <string>
#include "jiter.hh"
#include <ostream>

class propiter
{
protected:
  jiter<const std::list<Property*> > _it;
public:
  propiter(jiter<const std::list<Property*> > it)
    : _it(it) {}
  operator bool() { return _it; }
  Property& operator*() { return **_it; }
  Property* operator->() { return *_it; }
  void operator++() { ++_it; }
};

class Grammar
{
protected:
  CatTable cats;
  std::list<Property*> props;
  void _prop(Property* p) { props.push_back(p); }
public:
  Grammar(std::string& s) : cats(s), props() {}
  Grammar(const char* s) : cats(s), props()  {}

  int size() const { return cats.size(); }
  int sizeProp() const { return props.size(); }

  Grammar& operator<<(std::string& c) { cats << c; return *this; }
  Grammar& operator<<(const char* c) { cats << c; return *this; }

  int operator[](std::string& c) const { return cats[c]; }
  int operator[](const char* c) const { return cats[c]; }
  const std::string& operator[](int c) const { return cats[c]; }

  CatProxy con(int c) {
      Constituency* p =  new Constituency(c);
      _prop(p);
      return CatProxy(cats,p);
  }
  CatProxy con(std::string& c)
  { return con(cats[c]); }
  CatProxy con(const char* c)
  { return con(cats[c]); }

  void obl(int p, int c)
  { Obligation* ptr = new Obligation(p,c); _prop(ptr); }
  void obl(std::string& p, std::string& c)
  { obl(cats[p],cats[c]); }
  void obl(const char* p, const char* c)
  { obl(cats[p],cats[c]); }

  void uni(int p, int c)
  { Uniqueness* ptr = new Uniqueness(p,c); _prop(ptr); }
  void uni(std::string& p, std::string& c)
  { uni(cats[p],cats[c]); }
  void uni(const char* p, const char* c)
  { uni(cats[p],cats[c]); }

  void lin(int p, int c1, int c2)
  { Linearity* ptr = new Linearity(p,c1,c2); _prop(ptr); }
  void lin(std::string& p, std::string& c1, std::string& c2)
  { lin(cats[p],cats[c1],cats[c2]); }
  void lin(const char* p, const char* c1, const char* c2)
  { lin(cats[p],cats[c1],cats[c2]); }

  CatProxy req(int cf, int cc)
  { Requirement* p = new Requirement(cf, cc); _prop(p);
    return CatProxy(cats,p); }
  CatProxy req(std::string& cf, std::string& cc)
  { return req(cats[cf], cats[cc]); }
  CatProxy req(const char* cf, const char* cc)
  { return req(cats[cf], cats[cc]); }
 
  void exc(int p, int c1, int c2)
  { Exclusion* ptr = new Exclusion(p,c1,c2); _prop(ptr); }
  void exc(std::string& p, std::string& c1, std::string& c2)
  { exc(cats[p],cats[c1],cats[c2]); }
  void exc(const char* p, const char* c1, const char* c2)
  { exc(cats[p],cats[c1],cats[c2]); }

  void adj(int p, int c1, int c2)
  { Adjacence* ptr = new Adjacence(p,c1,c2); _prop(ptr); }
  void adj(std::string& p, std::string& c1, std::string& c2)
  { adj(cats[p],cats[c1],cats[c2]); }
  void adj(const char* p, const char* c1, const char* c2)
  { adj(cats[p],cats[c1],cats[c2]); }

  jiter<int> citer() const { return cats.iter(); }
  propiter piter() const
  { return propiter(jiter<const std::list<Property*> >(props)); }
};

#endif
