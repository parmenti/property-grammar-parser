## Property Grammar Parser

### About

Constraint-Based symbolic parser for Property Grammar described in [(Duchier et al., 2014)](https://hal.science/hal-00782398).

This parser is a proof-of-context released under the GPL License.

### Authors

Thi-Bich-Hanh Dao
Denys Duchier
Willy Lesaint
Yannick Parmentier

LIFO, Université d'Orléans

Copyright 2014

(See README file for technical information)
