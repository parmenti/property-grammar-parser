#include "digecode/model.hh"
#include "digecode/constraint.hh"
#include "digecode/space.hh"
#include "Tree.hh"
#include "Node.hh"
#include "Lexicon.hh"
#include "Grammar.hh"
#include "Property.hh"
#include <list>
#include <iostream>

using namespace std;
using namespace digecode;

// pour afficher les solutions
void Tree::print(ostream& cout, digecode::Model& m) const
{

  int r=-1;
  if (roots.assigned())
    r=roots.glbMin();
  cout << "---------------" << endl;
  cout << "root : " << roots << endl;
  for (SetVarGlbValues it(Vp); it(); ++it) {
    int i=it.val();
    if (i==r)
      cout << "*";
    cout << i << " ( ";
    IntVarValues icat(cat[i]);
    bool first = true;
    while (icat())
      {
	if (first) first=false;
	else cout << ",";
	cout << GRAM[icat.val()];
	++icat;
      }
    cout << " ) " << " : " << succ[i] << " | " << pred[i] << " | " << cols[i]<< endl;
  }
  cout << "satisfaction rate:" << sum_pert_and_sat << "/" << sum_pert << endl;
}

#ifndef PG_NO_GIST
RTDesc Tree::rtdesc(const Space& sol) const
{
  RTDesc desc(N,M);
  for (int r=0; r<N; ++r)
    for (int c=0; c<M; ++c)
      {
	int idx = r*M + c;
	IntVarValues icat(cat[idx]);
	ostringstream sout;
	bool first = true;
	while (icat())
	  {
	    if (first) first=false;
	    else sout << ",";
	    sout << GRAM[icat.val()];
	    ++icat;
	  }
	if (is_pos[idx].assigned())
	  if (is_pos[idx].val())
	    desc.push(r,c,1,sout.str());
	  else
	    desc.push(r,c,-1,"");
	else
	  desc.push(r,c,0,sout.str());
	SetVarGlbValues glb(succ[idx]);
	while (glb())
	  {
	    int idx2 = glb.val();
	    int r2 = idx2/M;
	    int c2 = idx2%M;
	    desc.push(r,c,r2,c2,1);
	    ++glb;
	  }
	SetVarUnknownValues unk(succ[idx]);
	while (unk())
	  {
	    int idx2 = unk.val();
	    int r2 = idx2/M;
	    int c2 = idx2%M;
	    desc.push(r,c,r2,c2,0);
	    ++unk;
	  }
      }
  return desc;
}
#endif
*/

// Tree methods for getting a niter

niter Tree::iter(int r1, int c1, int r2, int c2, int r, int c)
{ return niter(this, r1, c1, r2, c2, r, c); }

niter Tree::iter(int r1, int c1, int r2, int c2)
{ return iter(r1, c1, r2, c2, r1, c1); }

niter Tree::iter(int r1, int c1)
{ return iter(0, 0, N, M, r1, c1); }

niter Tree::iter(int idx)
{ return iter(idx/M, idx%M); }

niter Tree::iter()
{ return iter(0); }


//==============================================================================
// Tree constructor
//==============================================================================

Tree::Tree(int n, int m, char** words, Grammar& g, Lexicon& l, bool strong) :
  N(n), M(m), NM(N*M), V(0,NM-1), NCATS(g.size()), GRAM(g), LEX(l), WORDS(words), NPROPS(g.sizeProp()),
  succ        (*this, NM, IntSet::empty, V),
  succ_rec    (*this, NM, IntSet::empty, V),
  succ_eqrec  (*this, NM, IntSet::empty, V),
  pred        (*this, NM, IntSet::empty, V),
  pred_rec    (*this, NM, IntSet::empty, V),
  pred_eqrec  (*this, NM, IntSet::empty, V),
  roots       (*this, IntSet::empty, V, 1, 1), // with cardinality bound
  Vp          (*this, IntSet(0,M-1),V), // leaves are in Vp
  Vm          (*this, IntSet::empty,V),
  cols        (*this, NM, IntSet::empty, IntSet(0,M-1)),
  is_pos      (*this, NM, 0, 1),
  is_neg      (*this, NM, 0, 1),
  is_root     (*this, NM, 0, 1),
  cat         (*this, NM, 0, NCATS-1),
  has_cat     (*this, NM*NCATS, 0, 1),
  is_succ     (*this, NM*NM, 0, 1),
  is_pred     (*this, NM*NM, 0, 1),
  sum_pert    (*this, 0, Int::Limits::max),
  sum_pert_and_sat(*this, 0, Int::Limit::max)
{
  post(strong);
}

//==============================================================================
// Post all constraints
//==============================================================================

void Tree::post(bool strong)
{

  maximize(sum_pert_and_sat, sum_pert);

  // gives the constraints
  // Vp\uplus Vm = V
  rel(*this, Vp, SOT_DUNION, Vm, SRT_EQ, V);

  // V+ = roots \uplus {succ[i] | i\in V}
  {
    SetVarArgs args(NM+1);
    for (int i=0; i<NM; ++i)
      args[i] = succ[i];
    args[NM] = roots;
    rel(*this, SOT_DUNION, args, Vp);
  }

  // post constraints for each node
  FOR(*this, n)
    {
       n->post();
    }

  propiter pit(GRAM.piter());
  while (pit)
  {
    FOR(*this, n)
        {
        pit->post(n, strong);
        }
    ++pit;
  }

  //for lexicon
  niter nit = iter(0,0,1,M);
  for(char** c = WORDS ; nit ; ++c, ++nit)
  {
      rel(*this, nit->cat(), IRT_EQ, LEX[*c]);
  }
  IntVarArgs pertinents(pert.size());
  IntVarArgs pertsats  (pert_and_sat.size());

  for(int i = 0 ; i < pert.size() ; i++) {
      pertinents[i] = pert[i];
      pertsats[i] = pert_and_sat[i];
  }

  linear(*this, pertinents, IRT_EQ, sum_pert,ICL_DOM);
  linear(*this, pertsats, IRT_EQ, sum_pert_and_sat,ICL_DOM);

  // poser strategie de branchement
  /* strategie first-fail,
     choisir la variable non instanciee dont le domaine est le plus petit
     pour explorer les valeurs de ce domaine une par une, en 
     commencant par la plus petite */
  SetVarArgs args(2*NM);
  for (int i=0; i<NM; ++i)
    {
      args[i] = succ[i];
      args[NM+i] = pred[i];
    }
  branch(*this,args, SET_VAR_SIZE_MIN, SET_VAL_MIN_EXC);
  branch(*this,pert_and_sat, INT_VAR_SIZE_MIN, INT_VAL_MIN);
  branch(*this,is_pos, INT_VAR_SIZE_MIN, INT_VAL_MIN);
  branch(*this,   cat, INT_VAR_SIZE_MIN, INT_VAL_MIN);
}
