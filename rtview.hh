#ifndef _RTVIEW_HH_
#define _RTVIEW_HH_

#include <QGraphicsScene>
#include <QGraphicsView>
#include <list>
#include "digecode/space.hh"
#include "digecode/gist.hh"
#include <string>
#include "Tree.hh"

using namespace std;

//==============================================================================
// description of a rt
//==============================================================================

struct RTLine
{
  const int r1,c1,r2,c2,det;
  RTLine(int R1, int C1, int R2, int C2, int D)
    : r1(R1), c1(C1), r2(R2), c2(C2), det(D) {}
};

struct RTNode
{
  const int r,c,status;
  std::string label;
  RTNode(int R, int C, int S, std::string L)
    : r(R), c(C), status(S), label(L) {}
};

struct RTDesc
{
  const int NROWS;
  const int NCOLS;
  std::list<RTLine> succs;
  std::list<RTNode> nodes;

  RTDesc(int R, int C)
    : NROWS(R), NCOLS(C), succs(), nodes() {}

  void push(int r1, int c1, int r2, int c2, int d)
  { succs.push_back(RTLine(r1,c1,r2,c2,d)); }

  void push(int r, int c, int s, std::string l)
  { nodes.push_back(RTNode(r,c,s,l)); }

};

//==============================================================================
// view of an rt
//==============================================================================

class RTView : public QGraphicsView
{
  Q_OBJECT
  public:
  RTView(RTDesc&, RTView**);

protected:
  void resizeEvent(QResizeEvent* event);
  QGraphicsScene *scene;
  const int NCOLS;
  const int NROWS;
  static const int SEP = 10;
  virtual void closeEvent(QCloseEvent*);
  RTView** prev;
  RTView*  next;
};

//===================================
// rtdesc function
//===================================

#ifndef PG_NO_GIST
RTDesc rtdesc(const digecode::Model& sol)
{
  Tree* t = new Tree;
  RTDesc desc(t->N(sol),t->M(sol));
  for (int r=0; r<N; ++r)
    for (int c=0; c<M; ++c)
      {
        int idx = r*M + c;
        IntVarValues icat(cat[idx]);
        ostringstream sout;
        bool first = true;
        while (icat())
          {
            if (first) first=false;
            else sout << ",";
            sout << GRAM[icat.val()];
            ++icat;
          }
        if (is_pos[idx].assigned())
          if (is_pos[idx].val())
            desc.push(r,c,1,sout.str());
          else
            desc.push(r,c,-1,"");
        else
          desc.push(r,c,0,sout.str());
        SetVarGlbValues glb(succ[idx]);
        while (glb())
          {
            int idx2 = glb.val();
            int r2 = idx2/M;
            int c2 = idx2%M;
            desc.push(r,c,r2,c2,1);
            ++glb;
          }
        SetVarUnknownValues unk(succ[idx]);
        while (unk())
          {
            int idx2 = unk.val();
            int r2 = idx2/M;
            int c2 = idx2%M;
            desc.push(r,c,r2,c2,0);
            ++unk;
          }
      }
  return desc;
}
#endif

//==============================================================================
// inspector
//==============================================================================

class RTInspector: public Gecode::Gist::Inspector
{
protected:
  RTInspector(const std::string& name);
  virtual void finalize(void);
  virtual ~RTInspector(void);
  virtual std::string name(void);
  RTView* first;
  const std::string _name;
};

template <class S>
class ViewGrid : public RTInspector
{
public:
  ViewGrid(const std::string& name)
    : RTInspector(name) {}
  /*
  virtual void inspect(const Gecode::Space& node)
  {
      //RTDesc desc = static_cast<const S&>(node).rtdesc();
      (new RTView(desc, &first))->show();
  }
    */
};

#endif
