#include "Node.hh"
#include "digecode/int.hh"
#include "digecode/set.hh"

using namespace digecode;
using namespace std;

// Node methods for getting a niter

niter Node::iter(int r1, int c1, int r2, int c2, int r, int c)
{ return _tree->iter(r1, c1, r2, c2, r, c); }

niter Node::iter(int r1, int c1, int r2, int c2)
{ return _tree->iter(r1, c1, r2, c2); }

niter Node::iter(int r1, int c1)
{ return _tree->iter(r1, c1); }

niter Node::iter(int idx)
{ return _tree->iter(idx); }

niter Node::iter()
{ return _tree->iter(); }

niter Node::iterGT()
{ return _tree->iter(_idx+1); }

niter Node::iterDR()
{ return _tree->iter(0, col(), row(), M()); }

//==============================================================================
// post constraints for one Node
//==============================================================================

void Node::post()
{
  // contraintes pour chaque noeud

  IntSet _self = IntSet(idx(), idx());
  SetVar self(tree(), _self, _self);
  // succ_eqrec = self \uplus succ_rec
  rel(tree(), self, SOT_DUNION, succ_rec(), SRT_EQ, succ_eqrec());
  // rec = \uplus {succ_eqrec[j] | j\in succ}
  element(tree(), SOT_DUNION, all_succ_eqrec(), succ(), succ_rec());
  // pred_eqrec=self \uplus pred_rec
  rel(tree(), self, SOT_DUNION, pred_rec(), SRT_EQ, pred_eqrec());
  // pred = \uplus {pred_eqrec[j] | j\in pred}
  element(tree(), SOT_DUNION, all_pred_eqrec(), pred(), pred_rec());
  // at most 1 pred
  cardinality(tree(), pred(), 0, 1);

  // initialize: is_succ_of, is_pred_of
  FOR(*this, N2)
    {
      dom(tree(), succ(), SRT_SUP, N2.idx(), N2.is_succ_of(*this));
      dom(tree(), pred(), SRT_SUP, N2.idx(), N2.is_pred_of(*this));
      // w\in pred(w') == w'\in succ(w)
      rel(tree(), is_succ_of(N2), IRT_EQ, N2.is_pred_of(*this));
      // the other relation is posted by N2.post()
    }

  // initialize has_cat
  for (jiter<int> it(GRAM().citer()); it; ++it)
    {
      int c = *it;
      rel(tree(), cat(), IRT_EQ, c, has_cat(c));
    }

  // extra conditions for being root
    
  dom(tree(), all_roots(), SRT_SUP, idx(), is_root());
  // succ_eqrec(w) = Vp
  rel(tree(), succ_eqrec(), SRT_EQ, all_pos(), is_root());

  // les succ des feuilles sont vides
  if (row() == 0)
    cardinality(tree(), succ(), 0, 0);

  // is_pos, is_neg
  dom(tree(), all_pos(), SRT_SUP, idx(), is_pos());
  rel(tree(), is_pos(), IRT_NQ, is_neg());

  // si w\in Vm alors pred[w]==succ[w]==\empty
  {
    BoolVar b1(tree(), 0, 1);
    BoolVar b2(tree(), 0, 1);
    dom(tree(), pred(), SRT_EQ, IntSet::empty, b1);
    dom(tree(), succ(), SRT_EQ, IntSet::empty, b2);
    rel(tree(), is_neg(), IRT_LQ, b1);
    rel(tree(), is_neg(), IRT_LQ, b2);
  }

  // eqrecs of two nodes are either disjoint or one is a subset
  // of the other
  FORGT(*this, n2)
    {
      BoolVarArray b(tree(), 3, 0, 1);
      rel(tree(), succ_eqrec(), SRT_DISJ, n2->succ_eqrec(), b[0]);
      rel(tree(), succ_eqrec(), SRT_SUB , n2->succ_eqrec(), b[1]);
      rel(tree(), succ_eqrec(), SRT_SUP , n2->succ_eqrec(), b[2]);
      linear(tree(), b, IRT_EQ, 1);
    }
   
  // ensemble des projections (cols)
  // ici, pour les noeuds feuilles
  if (row() == 0)
    // cols(w_0j)={j}
    dom(tree(), cols(), SRT_EQ, idx());
  else
    {
      int ii = row(); // ligne
      int jj = col(); // colonne
      IntVar jjv(tree(), jj, jj);
      // cols(w_ij=\uplus{cols(w)| w\in succ(w_ij)}
      element(tree(), SOT_DUNION, all_cols(), succ(), cols());
      // min(cols(w_ij)) = j  <=>  w_ij \in Vp
      min(tree(), cols(), jjv, is_pos());
      // convex(cols(w_ij))
      convex(tree(), cols());
      // pred_rec et succ_req sont inclus dans des quartiers du plan
      int bd [ii*(M()-jj)];       // #ligne * #col
      int bdi = 0;
      int hg [(N()-1-ii)*(jj+1)]; // #ligne * #col
      int hgi = 0;
      for (int cl = 0 ; cl < ii ; cl++) 
	for (int cc = jj ; cc < M() ; cc++) 
	  bd[bdi++] = cl * M() + cc;
      for (int cl = ii+1 ; cl < N() ; cl++) 
	for (int cc = 0 ; cc <= jj ; cc++)
	  hg[hgi++] = cl * M() + cc;
      {      
	// succ_rec[i] \sub { w_lk, 0 <= l < i, j <= k < m}
	IntSet isbd(bd, bdi);
	dom(tree(), succ_rec(), SRT_SUB, isbd);
      }
      {
	// pred_rec[i] \sub { w_lk, i < l < n, 0 <= k <= j}
	IntSet ishg(hg, hgi); 
	dom(tree(), pred_rec(), SRT_SUB, ishg);
      }

      // pas de ligne inoccupee:
      // w_ij\in Vp == (i-1)\in{l(w)|w\in succ(w_ij)}
      int succligne[M()-jj];
      int sli = 0;
      for (int x = jj ; x < M() ; x++)
	succligne[sli++] = (ii-1) * M() + x;
      IntSet sl(succligne, sli);
      SetVar svsl(tree(), sl, sl);
      SetVar sinter(tree(), IntSet::empty, sl);
      rel(tree(), succ(), SOT_INTER, svsl, SRT_EQ, sinter);
      dom(tree(), sinter, SRT_NQ, IntSet::empty, is_pos());
    }

  // si n est dans Vm alors cat vaut none
  rel(tree(), cat(), IRT_EQ, GRAM("none"), is_neg());
}
