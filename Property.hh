#ifndef _PROPERTY_HH_
#define _PROPERTY_HH_

#include <map>
#include <cassert>
#include <list>
#include "jiter.hh"
#include <string>

//==============================================================================
// CatTable
//
// CatTable cats;
// cats << "none" << "S" << "NP" << "VP" << "Prep" << "Verb";
// cats["S"] ==> 1
// cats[1] ==> "S"
//==============================================================================

class CatTable
{
protected:
  std::map<std::string,int> _encode;
  std::map<int,std::string> _decode;
  int _counter;
public:
  void enter(std::string& cat)
  {
    _encode[cat] = _counter;
    _decode[_counter] = cat;
    ++_counter;
  }

  CatTable& operator<<(std::string& cat)
  { enter(cat); return *this; }

  CatTable& operator<<(const char* cat)
  { std::string s(cat); enter(s); return *this; }

  CatTable(std::string& s) : _encode(), _decode(), _counter(0)
  { *this << s; }
  CatTable(const char* s) : _encode(), _decode(), _counter(0)
  { *this << s; }

  int size() const { return _counter; }

  int operator[](std::string& cat) const
  {
    std::map<std::string,int>::const_iterator it(_encode.find(cat));
    assert(it != _encode.end());
    return it->second;
  }

  int operator[](const char* ptr) const
  {
    std::string cat(ptr); return (*this)[cat];
  }

  const std::string& operator[](int code) const
  {
    std::map<int,std::string>::const_iterator it(_decode.find(code));
    assert(it != _decode.end());
    return it->second;
  }

  jiter<int> iter() const { return jiter<int>(_counter); }
};

//==============================================================================
// Properties
//==============================================================================

class Node;

class Property
{
public:
  virtual void post(Node&, bool) = 0;
};

class PropertyList : public Property
{
protected:
  std::list<int> _children;
public:
  PropertyList() : Property(), _children() {}
  std::list<int>& getChildren() {return _children;}
  jiter<std::list<int> > children()
  { return jiter<std::list<int> >(_children); }
  int size() const { return _children.size(); }
};

class Constituency: public PropertyList
{
protected:
  const int _parent;
  //std::list<int> _children; // NOW inherited from PropertyList to share proxy
public:
  friend class CatProxy;
  Constituency(int p) : _parent(p) {}
  int parent() const { return _parent; }
  virtual void post(Node&,bool);
};

class CatProxy
{
protected:
  CatTable& cats;
  PropertyList*const prop;
public:
  CatProxy(CatTable& c, PropertyList* p)
    : cats(c), prop(p) {}
  CatProxy& operator<<(int c)
  { prop->getChildren().push_back(c); return *this; }
  CatProxy& operator<<(std::string& c)
  { return *this << cats[c]; }
  CatProxy& operator<<(const char* c)
  { return *this << cats[c]; }
};

class Obligation: public Property
{
protected:
  const int _parent;
  const int _child;
public:
  Obligation(int p, int c) : _parent(p), _child(c) {}
  int parent() const { return _parent; }
  int child() const { return _child; }
  virtual void post(Node&,bool);
};

class Uniqueness: public Property
{
protected:
  const int _parent;
  const int _child;
public:
  Uniqueness(int p, int c) : _parent(p), _child(c) {}
  int parent() const { return _parent; }
  int child() const { return _child; }
  virtual void post(Node&,bool);
};

class Linearity : public Property
{
protected:
  const int _parent;
  const int _child1;
  const int _child2;
public:
  Linearity(int p, int c1, int c2) : _parent(p), _child1(c1), _child2(c2) {}
  int parent() const { return _parent; }
  int child1() const { return _child1; }
  int child2() const { return _child2; }
  virtual void post(Node&,bool);
};

class Requirement : public PropertyList
{
protected:
  const int _parent;
  const int _child;
  //std::list<int> _children; // INHERITED
public:
  friend class CatProxy;
  Requirement(int p, int c) : _parent(p), _child(c) {}
  int parent() const { return _parent; }
  int child() const { return _child; }
  virtual void post(Node&,bool);
};

class Exclusion : public Property
{
protected:
  const int _parent;
  const int _child1;
  const int _child2;
public:
  Exclusion(int p, int c1, int c2) : _parent(p), _child1(c1), _child2(c2) {}
  int parent() const { return _parent; }
  int child1() const { return _child1; }
  int child2() const { return _child2; }
  virtual void post(Node&,bool);
};

class Adjacence : public Property
{
protected:
  const int _parent;
  const int _child1;
  const int _child2;
public:
  Adjacence(int p, int c1, int c2) : _parent(p), _child1(c1), _child2(c2) {}
  int parent() const { return _parent; }
  int child1() const { return _child1; }
  int child2() const { return _child2; }
  virtual void post(Node&,bool);
};

#endif
