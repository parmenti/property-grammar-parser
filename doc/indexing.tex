\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amstext,amssymb}
\usepackage[french,english]{babel}
\usepackage{graphicx}
\usepackage{pgf,tikz}
\usepackage{float}
\selectlanguage{english}
\StandardLayout
\setlength{\parindent}{0pt}
\setlength{\parskip}{\medskipamount}
\newcommand{\DR}{\textsf{DR}}
\newcommand{\NI}{\textsf{N}}
\newcommand{\OBL}{{\text{\textsf{obl}}}}
\newcommand{\CON}{{\text{\textsf{con}}}}
\newcommand{\LIN}{{\text{\textsf{lin}}}}
\newcommand{\N}{{\mathbb{N}}}
%
\newcommand{\inst}[1]{\langle#1\rangle}
\newcommand{\posij}[1]{\text{pos}_{(i,j)}#1}
\newcommand{\ind}{\text{ind}}
%
\begin{document}
\begin{center}
  \textbf{\textsf{Indexing Property Instances}}\\[1ex]
  \today
\end{center}
\vspace*{0.5cm}
\paragraph{Context.} 
We assume a grid with $N$ lines and $M$ columns. Such a grid contains 
$N \times M$ cells, each of these being a candidate node of a model. A
model is a {\it tree}\footnote{On top of properties on node labels,
  constraints ensuring tree-shapeness are applied. In particular,
  the column (coordinate on the x-axis) of a daughter node is always
  greater than or equal to the column of its mother.} satisfying
properties on node labels. Such a grid is depicted on
Fig.~\ref{fig:grid0}. Each property is evaluated at every node of
the grid. This evaluation consists of checking whether the property is
pertinent (and then satisfied) at a given node. 

\begin{figure}[h]
  \begin{center}
    \input grid0.tex 
  \end{center}
  \caption{Coordinates of nodes on the grid.}
  \label{fig:grid0}
\end{figure}

In order to count the number of properties that are satisfied in a
given tree model, we need to store the evaluation of every property at
every node of the model (boolean values). For some reasons (including
efficiency when solving constraints), we choose to store these values
in a global array. In this context, we need to compute indexes to
access the evaluation of a given property at a given node in this array.

In our indexing scheme, we will consider 3 types of property\,:
%% \begin{enumerate}
%% \item those whose pertinence is evaluated at a given node, such as 
%%   {\it obligation},
%% \item those whose pertinence is evaluated at a given couple of nodes,
%%   such as {\it constituency}, or {\it requirement},
%% \item those whose pertinence is evaluated at a given triple of nodes,
%%   such as {\it linearity}, {\it uniqueness} or {\it exclusion}.
%% \end{enumerate}
\begin{enumerate}
\item Instances of the obligation property: those of the form $\inst{(i,j)}$,  where $(i,j)$ is a node.
\item Instances of the requirement and the constituency properties: those of the form $\inst{(i,j),(i',j')}$, where $(i,j)$ is a node and $(i',j')$ is a daughter node of $(i,j)$.
\item Instances of the linearity, the uniqueness and the exclusion properties: those of the form $\inst{(i,j),(i',j'),(i'',j'')}$, where $(i,j)$ is a node and $(i',j')$ and $(i'',j'')$ are distinct daughter nodes of $(i,j)$.
\end{enumerate}

If $p$ is a property, we write $\NI_p(i,j)$ for the number of
instances of this property at node $(i,j)$.  We write $\NI_p^*$ for
the total number of instances of this property on the grid. We write
$\NI_p^<(i,j)$ for the number of instances for all nodes preceding
$(i,j)$, i.e.\ for all $(i',j')$ such that either $i'<i$ or
$i'=i\wedge j'<j$. We will use $\NI_p^<(i,j)$ as the offset for
the instances of $p$ at node $(i,j)$.

\paragraph{Obligation.}
This property illustrates the basic indexing scheme. For each node
$(i,j)$, one instance of the property is created (the obligation is
satisfied or not at a given node). Therefore\,:  
\begin{align*}
\NI_\OBL(i,j) &= 1
\end{align*}

Furthermore\,:
\begin{align*}
\NI_\OBL^* &= \mathop\Sigma_{i\in[0,N)}\mathop\Sigma_{j\in[0,M)} 1 
= \mathop\Sigma_{i\in[0,N)} M = N \times M
\end{align*}

The offset that allows to access the evaluation of the obligation
property is then\,:
\begin{align*}
\NI_\OBL^<(i,j) &= \mathop\Sigma_{l=0}^{i-1}\mathop\Sigma_{c=0}^{M-1}
1 + \mathop\Sigma_{c=0}^{j-1} 1 = i \times M + j
\end{align*}

Given an instance $\inst{(i,j)}$, its index in the table of instances
of the property is\,:
\begin{align*}
\ind_\OBL(\inst{(i,j)}) = i \times M + j
\end{align*}

\paragraph{Introducing DR blocks.}
When evaluating the pertinence of a given property, we only consider
the label of a given node with respect to the labels of its daughter 
nodes. 
Given a node $(i,j)$, we write $\DR(i,j)$ for the block $[0,i) \times
  [j,M)$ (that is, the cells of the  grid, which may host a 
daughter node of the node $(i,j)$, see Fig.~\ref{fig:grid}).
%
\begin{figure}[H]
  \begin{center}
    %\includegraphics[width=0.7\textwidth]{grid.pdf}
    \input grid1.tex
    
    \vspace*{-0.2cm}
  \end{center}
  \caption{DR block of a given node in the grid.}
  \label{fig:grid}
\end{figure}
%
For properties whose pertinence is evaluated at more than a node, we
need to consider the potential daughter nodes. Let $n''$ be such a
daughter node of node $n$. Imagine we want to store in our array the
evaluation of a constituency property with respect to $(n,n'')$, we
need to compute a shift (see Fig.~\ref{fig:linear}).
%
\begin{figure}[htbp]
  \begin{center}
    %\includegraphics[width=0.8\textwidth]{linear.pdf}
    \input linear.tex
    
    \vspace*{-0.8cm}
  \end{center}
  \caption{Indexing with respect to pair of nodes.}
  \label{fig:linear}
\end{figure}
%
\newpage
\paragraph{Constituency.}  For each node $(i,j)$, one instance is created for
every node in $\DR(i,j)$.  Therefore:
\begin{align*}
\NI_\CON(i,j) &= i(M-j)
\end{align*}
We deduce the total number of instances:
\begin{align*}
\NI_\CON^* &= \mathop\Sigma_{i\in[0,N)}\mathop\Sigma_{j\in[0,M)} i(M-j)\\
&= \mathop\Sigma_{i\in[0,N)} i((M-0)+(M-1)+\cdots+1)\\
&= \mathop\Sigma_{i\in[0,N)} i M (M+1) / 2\\
&= N (N-1) M (M+1) / 4
\end{align*}
Now the offset at $(i,j)$:
\begin{align*}
\NI_\CON^<(i,j) &= (\mathop\Sigma_{i'\in[0,i)}\mathop\Sigma_{j'\in[0,M)} i'(M-j'))
    + \mathop\Sigma_{j'\in[0,j)} i(M-j')\\
&= i(i-1)M(M+1)/4 + ij(2M-j+1)/2
\end{align*}

Given an instance $\inst{(i,j),(i',j')}$, its index in the table of
the instances of the property $\CON$ is\,:
\begin{align*}
\ind_\CON(\inst{(i,j),(i',j')})&=\NI_\CON^<(i,j)+\posij{(i',j')}\\
&=\NI_\CON^<(i,j)+ i'(M-j)+j'-j 
\end{align*}


\paragraph{Linearity.}  For each node $(i,j)$, one instance is created for every
pair $(n_1,n_2)$ of nodes in $\DR(i,j)$ such that $n_1\neq n_2$.  Therefore:
\begin{align*}
\NI_\LIN(i,j) &= i(M-j)(i(M-j) - 1)\\
&= i^2(M-j)^2 - i(M-j)
\end{align*}
We deduce the total number of instances:
\begin{align*}
\NI_\LIN^* &= \mathop\Sigma_{i\in[0,N)}\mathop\Sigma_{j\in[0,M)} i^2(M-j)^2 - i(M-j)
\end{align*}
To proceed, we need the formula for the sum of squares of a prefix of $\N$:
\[
\begin{array}{r@{{}={}}r@{{}+{}}r@{{}+{}}r@{{}+{}}c}
(n+1)^3 & n^3 & 3n^2 & 3n & 1\\
n^3 & (n-1)^3 & 3(n-1)^2 & 3(n-1) & 1\\
(n-1)^3 & (n-2)^3 & 3(n-2)^2 & 3(n-2) & 1\\
\multicolumn{1}{c@{{}\vdots{}}}{} & \multicolumn{4}{c}{}\\
3^3 & 2^3 & 3\times 2^2 & 3\times 2 & 1\\
2^3 & 1^3 & 3\times1^2 & 3\times 1 & 1\\\hline
(n+1)^3 & 1 & 3(\Sigma_{i=1}^{i=n}i^2) & 3(\Sigma_{i=1}^{i=n}i) & n
\end{array}
\]
\begin{align*}
3\Sigma_{i=1}^{i=n}i^2 &= (n+1)^3 - 1 - 3n(n+1)/2 - n\\
6\Sigma_{i=1}^{i=n}i^2 &= 2(n^3 + 3n^2 + 3n + 1) - 2 - 3(n^2+n) - 2n\\
&= 2n^3 + 3n^2 + n\\
&= n(2n+1)(n+1)\\
\Sigma_{i=1}^{i=n}i^2 &= n(2n+1)(n+1)/6
\end{align*}
Let's return to the total number of instances:
\begin{align*}
\NI_\LIN^* &= \mathop\Sigma_{i\in[0,N)}(\mathop\Sigma_{j\in[0,M)} i^2(M-j)^2 -
    i(M-j))\\
&= \mathop\Sigma_{i\in[0,N)}((i^2\mathop\Sigma_{j\in[0,M)}(M-j)^2) -
(i\mathop\Sigma_{j\in[0,M)}(M-j)))\\
&=\mathop\Sigma_{i\in[0,N)}(i^2(M^2+(M-1)^2+\cdots+1)
- i(M+(M-1)+\cdots+1))\\
&= \mathop\Sigma_{i\in[0,N)}(i^2M(2M+1)(M+1)/6 - iM(M+1)/2)\\
&= ((N-1)(2(N-1)+1)((N-1)+1)/6)(M(2M+1)(M+1)/6)\\
&\qquad - (N(N-1)/2)(M(M+1)/2)\\
&=N(2N-1)(N-1)M(2M+1)(M+1)/36 - N(N-1)M(M+1)/4
\end{align*}
Now the offset at $(i,j)$.  By definition:
{\allowdisplaybreaks
\begin{align*}
\NI_\LIN^<(i,j) &= (\mathop\Sigma_{i'\in[0,i)}\mathop\Sigma_{j'\in[0,M)}\NI_\LIN(i',j'))
+ \mathop\Sigma_{j'\in[0,j)}\NI_\LIN(i,j')\\
%
%% &= (\mathop\Sigma_{i'\in[0,i)}\mathop\Sigma_{j'\in[0,M)}i'^2(M-j')^2-i'(M-j'))
%% \\&\qquad+ \mathop\Sigma_{j'\in[0,j)}i^2(M-j')^2-i(M-j')\\
%% &=(\mathop\Sigma_{i'\in[0,i)} i'^2(M^2+(M-1)^2+\cdots+1) -
%%     i'(M+(M-1)+\cdots+1))\\
%% &\qquad+i^2(M^2+(M-1)^2+\cdots+(M-j+1))\\
%% &\qquad-i(M+(M-1)+\cdots+(M-j+1))\\
%% &=(\mathop\Sigma_{i'\in[0,i)} i'^2M(2M+1)(M+1)/6 -
%%     i'M(M+1)/2)\\
%% &\qquad+i^2(M^2+(M-1)^2+\cdots+(M-j+1))\\
%% &\qquad-ij(2M-j+1)/2\\
%% &= ((i-1)(2(i-1)+1)((i-1)+1)/6)(M(2M+1)(M+1)/6)\\
%% &\qquad- (i(i-1)/2)(M(M+1)/2)\\
%% &\qquad+i^2(M^2+(M-1)^2+\cdots+(M-j+1))\\
%% &\qquad-ij(2M-j+1)/2\\
%% &= i(2i-1)(i-1)M(2M+1)(M+1)/36\\
%% &\qquad+i^2(M^2+(M-1)^2+\cdots+(M-j+1))\\
%% &\qquad-ij(2M-j+1)/2
%% \end{align*}
%% }
%% We need the formula for the sum $M^2+(M-1)^2+\cdots+(M-j+1)$:
%% \[
%% \begin{array}{r@{{}={}}r@{{}+{}}r@{{}+{}}r@{{}+{}}c}
%% (M+1)^3 & M^3 & 3M^2 & 3M & 1\\
%% M^3 & (M-1)^3 & 3(M-1)^2 & 3(M-1) & 1\\
%% (M-1)^3& (M-2)^3 & 3(M-2)^2 & 3(M-2) & 1\\
%% \multicolumn{1}{c@{{}\vdots{}}}{} & \multicolumn{4}{c}{}\\
%% (M-j+2)^3 & (M-j+1)^3 & 3(M-j+1)^2 & 3(M-j+1) & 1\\\hline
%% (M+1)^3 & (M-j+1)^3 & 3(\Sigma_{i=M-j+1}^{i=M} i^2) & 3(\Sigma_{i=M-j+1}^{i=M}i)
%% &  j
%% \end{array}
%% \]
%% \begin{align*}
%% 3\Sigma_{i=M-j+1}^{i=M} i^2 &= (M+1)^3 - (M-j+1)^3 - 3j(2M-j+1)/2 - j
%% \end{align*}
%% etc\ldots
%
\end{align*}
For the first operand, in the same way as the calculus of $\NI_\LIN^*$
and by replacing $N$ by $i$, we have:  
\begin{align*}
\mathop\Sigma_{i'\in[0,i)}\mathop\Sigma_{j'\in[0,M)}\NI_\LIN(i',j')&=
i(2i-1)(i-1)M(2M+1)(M+1)/36 - i(i-1)M(M+1)/4
\end{align*}
And for the second:
\begin{align*}
\mathop\Sigma_{j'\in[0,j)}\NI_\LIN(i,j')&= \mathop\Sigma_{j'\in[0,j)}(i^2(M-j')^2-i(M-j'))\\
&=i^2\mathop\Sigma_{j''=M-j+1}^{M} j''^2-i \mathop\Sigma_{j''=M-j+1}^{M} j''\\
&=i^2(\mathop\Sigma_{j''=1}^{M} j''^2-\mathop\Sigma_{j''=1}^{M-j} j''^2)-i (M-(M-j+1)+1)(M+M+j-1)/2\\
&=i^2(M(M+1)(2M+1)/6-(M-j)(M-j+1)(2M-2j+1)/6)\\
&\qquad-ij(2M-j+1)/2
\end{align*}
The offset $\NI_\LIN^<(i,j)$ is then
\begin{align*}
\NI_\LIN^<(i,j) &=
i(2i-1)(i-1)M(2M+1)(M+1)/36 - i(i-1)M(M+1)/4\\
& \qquad +i^2(M(M+1)(2M+1)/6-(M-j)(M-j+1)(2M-2j+1)/6)\\
&\qquad-ij(2M-j+1)/2
\end{align*}
Given an instance $\inst{(i,j),(i',j'),(i'',j'')}$, we need now to
calculate its index in the table of instances of the property
$\LIN$. This index depends on the fact that $(i'',j'')$ precedes
$(i',j')$ in the bloc $\DR(i,j)$ or not. 
\begin{itemize}
\item If $(i'',j'')$ precedes $(i',j')$ in $\DR(i,j)$, that is $\posij{(i'',j'')}<\posij{(i',j')}$: in this case, the index $\ind_\LIN(\inst{(i,j),(i',j'),(i'',j'')})$ is
\begin{align*}
&\NI_\LIN^<(i,j) + \posij{(i',j')}(i(M-j)-1)+\posij{(i'',j'')}\\
=&\NI_\LIN^<(i,j) + (i'(M-j)+j'-j)(i(M-j)-1)+i''(M-j)+j''-j
\end{align*}
\item Otherwise, the node $(i',j')$ precedes $(i'',j'')$. Since
  $(i'',j'')$ must be distinct from $(i',j')$, the node $(i',j')$ must
  not be counted as a possible element in the range for
  $(i'',j'')$. Therefore the index must be decremented by 1 wrt to the
  other case. The index $\ind_\LIN(\inst{(i,j),(i',j'),(i'',j'')})$ is: 
\begin{align*}
&\NI_\LIN^<(i,j) + \posij{(i',j')}(i(M-j)-1)+\posij{(i'',j'')}-1
\end{align*}
\end{itemize}

\end{document}
