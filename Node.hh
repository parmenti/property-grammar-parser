#ifndef _NODE_HH_
#define _NODE_HH_

#include "digecode/model.hh"
#include "digecode/constraint.hh"
#include "digecode/space.hh"
#include "Tree.hh"
#include <string>
#include "Grammar.hh"

using namespace digecode;
class niter;

//==============================================================================
// Node
//==============================================================================

class Node
{
protected:
  Tree*const _tree;
  int _idx;
public:
  Node(Tree* tree, int idx) : _tree(tree), _idx(idx) {}

  int N () const { return _tree->N; }
  int M () const { return _tree->M; }
  int NM() const { return _tree->NM; }
  int NCATS() const { return _tree->NCATS; }

  const Grammar& GRAM() const { return _tree->GRAM; }
  int GRAM(std::string& s) const { return _tree->GRAM[s]; }
  int GRAM(const char* s) const { return _tree->GRAM[s]; }
  const std::string& GRAM(int c) const { return _tree->GRAM[c]; }
  int NPROPS() const { return _tree->NPROPS; }

  int row() const { return _idx/M(); }
  int col() const { return _idx%M(); }
  int idx() const { return _idx; }

  Tree& tree() const { return *_tree; }

  SetVar& succ      () const { return _tree->succ      [_idx]; }
  SetVar& succ_rec  () const { return _tree->succ_rec  [_idx]; }
  SetVar& succ_eqrec() const { return _tree->succ_eqrec[_idx]; }
  SetVar& pred      () const { return _tree->pred      [_idx]; }
  SetVar& pred_rec  () const { return _tree->pred_rec  [_idx]; }
  SetVar& pred_eqrec() const { return _tree->pred_eqrec[_idx]; }
  SetVar& cols      () const { return _tree->cols      [_idx]; }

  BoolVar& is_pos () const { return _tree->is_pos [_idx]; }
  BoolVar& is_neg () const { return _tree->is_neg [_idx]; }
  BoolVar& is_root() const { return _tree->is_root[_idx]; }

  BoolVar& has_cat(int cat) const { return _tree->has_cat[NCATS()*_idx+cat]; }

  BoolVar& is_succ_of(Node& other) const { return _tree->is_succ[NM()*other.idx()+_idx]; }
  BoolVar& is_pred_of(Node& other) const { return _tree->is_pred[NM()*other.idx()+_idx]; }

  IntVar&      cat() const { return _tree->cat[_idx]; }

  SetVarArray& all_succ      () const { return _tree->succ      ; }
  SetVarArray& all_succ_rec  () const { return _tree->succ_rec  ; }
  SetVarArray& all_succ_eqrec() const { return _tree->succ_eqrec; }
  SetVarArray& all_pred      () const { return _tree->pred      ; }
  SetVarArray& all_pred_rec  () const { return _tree->pred_rec  ; }
  SetVarArray& all_pred_eqrec() const { return _tree->pred_eqrec; }
  SetVarArray& all_cols      () const { return _tree->cols      ; }

  IntVarArray& all_cat       () const { return _tree->cat       ; }

  SetVar& all_roots() const { return _tree->roots; }
  SetVar& all_pos  () const { return _tree->Vp; }
  SetVar& all_neg  () const { return _tree->Vm; }

  vector<BoolVar>& pert        () const { return _tree->pert; }
  vector<BoolVar>& pert_and_sat() const { return _tree->pert_and_sat; }

  niter iter(int r1, int c1, int r2, int c2, int r, int c);
  niter iter(int r1, int c1, int r2, int c2);
  niter iter(int r1, int c1);
  niter iter(int idx);
  niter iter();
  niter iterGT();
  niter iterDR();

  void post();

  int sizeDR() const
  {
    return row() * (M() - col());
  }
};

//==============================================================================
// Node iterator
//==============================================================================

class niter : public Node
{
protected:
  // parameters of the slice
  const int R1;
  const int C1;
  const int R2;
  const int C2;
  // position in the slice
  int R;
  int C;

  inline void load()
  {
    _idx = R*M() + C;
  }
  
  inline void advance()
  {
    C += 1;
    if (C >= C2)
      {
	C=C1;
	R += 1;
      }
    load();
  }
public:
  niter(Tree* t, int r1, int c1, int r2, int c2, int r, int c)
    : Node(t, 0),
      R1(r1), C1(c1), R2(r2), C2(c2),
      R(r), C(c)
  {
    load();
  }
  operator bool() const
  {
    return R<R2;
  }
  Node& operator*() { return *this; }
  Node* operator->(){ return  this; }
  void operator++() { advance(); }
};

//==============================================================================
// Macros
//==============================================================================

#define FOR(T,N) \
  for (niter N((T).iter()); N; ++N)

#define FORNQ(N1,N2) \
  for (niter N2((N1).iter()); N2; ++N2) \
    if ((N1).idx() != N2.idx())

#define FORGT(N1,N2) \
  for (niter N2((N1).iterGT()); N2; ++N2)

#define FORDR(N1,N2) \
  for (niter N2((N1).iterDR()); N2; ++N2)

#define FOR2(T,N1,N2) \
  FOR(T, N1) FOR(T, N2)

#define FOR2NQ(T,N1,N2) \
  FOR(T, N1) FORNQ(N1, N2)

#define FOR2GT(T,N1,N2) \
  FOR(T, N1) FORGT(N1, N2)

#define FOR2DR(T,N1,N2) \
  FOR(T, N1) FORDR(N1, N2)

#define FORDRDR(T,N1,N2) \
  FORDR(T,N1) FORDR(T,N2)

#endif
