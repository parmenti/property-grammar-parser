#ifndef _TREE_HH_
#define _TREE_HH_

#include "digecode/model.hh"
#include "digecode/constraint.hh"
#include "digecode/space.hh"
#include "config.hh"
#include <iostream>
#include <vector>
#include "Property.hh"
#include "Grammar.hh"
#include "Lexicon.hh"

using namespace digecode;

class niter;

struct Tree : Model
{
public:
  const int N; // number of lines
  const int M; // number of columns
  const int NM; // number of nodes
  const IntSet V;
  const int NCATS;
  const Grammar& GRAM;
  Lexicon& LEX;
  char** WORDS;
  const int NPROPS;
  SetVarArray succ;
  SetVarArray succ_rec;
  SetVarArray succ_eqrec;
  SetVarArray pred;
  SetVarArray pred_rec;
  SetVarArray pred_eqrec;
  SetVar roots;
  SetVar Vp; // V+
  SetVar Vm; // V-
  SetVarArray cols;
  BoolVarArray is_pos;
  BoolVarArray is_neg;
  BoolVarArray is_root;
  IntVarArray  cat;
  BoolVarArray has_cat;
  BoolVarArray is_succ;
  BoolVarArray is_pred;
  std::vector<BoolVar> pert;
  std::vector<BoolVar> pert_and_sat;
  IntVar sum_pert;
  IntVar sum_pert_and_sat;

  Tree(int, int, char** words, Grammar&, Lexicon&, bool strong=true);
  void post(bool);

  niter iter(int r1, int c1, int r2, int c2, int r, int c);
  niter iter(int r1, int c1, int r2, int c2);
  niter iter(int r1, int c1);
  niter iter(int idx);
  niter iter();

  virtual void constrain(const Space& sol);

  // to print the solutions
  void print(std::ostream& cout, digecode::Model& m) const;

  /*
#ifndef PG_NO_GIST
  // computes the description for the QGraphicsView
  RTDesc rtdesc(const Space& sol) const;
#endif
  */
};

#endif
