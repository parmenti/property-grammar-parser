#include "Property.hh"
#include "Node.hh"
#include "digecode/model.hh"
#include "digecode/constraint.hh"
#include "digecode/space.hh"
#include "gecode/int.hh"
#include "gecode/set.hh"
#include <iostream>

using namespace digecode;
using namespace std;

void Constituency::post(Node& node, bool strong)
{
  /*
    A:S?
    IF there is a node with cat A THEN
    the cat of any child is one of the set S
  */
  // collect the categories allowed for children
  int allowed_cats[size()];
  
  jiter<std::list<int> > citer(children());
  for (int i=0; citer; ++i, ++citer)
    {
      allowed_cats[i] = *citer;
    }

  // create the corresponding IntSet
  IntSet cs(allowed_cats, size());

  Tree& tree = node.tree();

  FORDR(node, other)
    {
      BoolVar pertinent(tree, 0, 1);
      BoolVar pertsat  (tree, 0, 1);

      BoolVar other_has_allowed_cat(tree, 0, 1);
      dom(tree, other.cat(), cs, other_has_allowed_cat);
      //pertinence:
      rel(tree, node.has_cat(parent()), BOT_AND, other.is_succ_of(node), pertinent);
      //pertinence and satisfaction:
      rel(tree, node.pert(index), BOT_AND, other_has_allowed_cat, pertsat);
      // strong semantics: pertinence => pertinence and satisfaction
      if (strong)
        rel(tree, pertinent, BOT_IMP, pertsat, 1);
      node.pert().push_back(pertinent);
      node.pert_and_sat().push_back(pertsat);
  }
}

void Obligation::post(Node& node, bool strong)
{
  /*
    A:^B
    IF there is a node with cat A THEN 
    there is at least one child with cat B
  */
  Tree& tree = node.tree();
  BoolVarArray ok(tree, node.sizeDR(), 0, 1);
  int i = 0;

  FORDR(node, other)
    {
      rel(tree,
	  other.is_succ_of(node), BOT_AND,
	  other.has_cat(child()), ok[i]);
      ++i;
    }
  BoolVar pertinent(tree, 0, 1);
  BoolVar pertsat  (tree, 0, 1);
  //pertinence:
  rel(tree, node.has_cat(parent()), IRT_EQ, 1, pertinent);
  //pertinence and satisfaction:
  BoolVar at_least_one(tree, 0, 1);
  rel(tree, BOT_OR, ok, at_least_one);
  rel(tree, node.pert(index), BOT_AND, at_least_one, pertsat);
  //strong semantics:
  if (strong)
    rel(tree, node.pert(index), BOT_IMP, node.pert_and_sat(index), 1);
  node.pert().push_back(pertinent);
  node.pert_and_sat().push_back(pertsat);
}

void Uniqueness::post(Node& node, bool strong)
{
  /*
    A:B!
    IF there is a node with cat A THEN 
    there is at most one child with cat B
  */
  Tree& tree = node.tree();

  FORDR(node, other1) {
      FORDR(node, other2) {
        if (other1.idx() != other2.idx()) {
          BoolVarArgs ok(5);
          ok[0] = other1.is_succ_of(node);
          ok[1] = other1.has_cat(child());
          ok[2] = other2.is_succ_of(node);
          ok[3] = other2.has_cat(child());
          ok[4] = node.has_cat(parent());

          BoolVar pertinent(tree, 0, 1);
          BoolVar pertsat  (tree, 0 ,1);
          // pertinence
          rel(tree, BOT_AND, ok, pertinent);
          // pertinence and satisfaction
          BoolVar sat(tree, 0, 1);
          rel(tree, sat, IRT_EQ, (other1.idx() == other2.idx()) ? 1 : 0);
          rel(tree, pertinent, BOT_AND, sat, pertsat);
          if (strong)
            rel(tree, pertinent, BOT_IMP, pertsat, 1);
          node.pert().push_back(pertinent);
          node.pert_and_sat().push_back(pertsat);
        }
      }
    }
}

void Linearity::post(Node& node, bool strong)
{
  /*
    A:B < C
    IF there is a node with cat A and there are two children with cat B and C THEN
    B precedes C (linear order)
  */
  Tree& tree = node.tree();

  FORDR(node, other1) {
    FORDR(node, other2) {
          if (other1.idx() == other2.idx())
            continue;
          BoolVarArgs ok(5);
          ok[0] = other1.is_succ_of(node);
          ok[1] = other1.has_cat(child1());
          ok[2] = other2.is_succ_of(node);
          ok[3] = other2.has_cat(child2());
          ok[4] = node.has_cat(parent());
	      
          BoolVar pertinent(tree, 0, 1);
          BoolVar pertsat  (tree, 0, 1);
          // pertinence
          rel(tree, BOT_AND, ok, pertinent);
          // pertinence and satisfaction
          BoolVar  sat(tree, 0, 1);
          rel(tree, sat, IRT_EQ, (other1.col() < other2.col() ? 1 : 0));
          rel(tree, pertinent, BOT_AND, sat, pertsat);
          if (strong)
            rel(tree, pertinent, BOT_IMP, pertsat, 1);
          node.pert().push_back(pertinent);
          node.pert_and_sat().push_back(pertsat);
          }
  }
}

void Requirement::post(Node& node, bool strong)
{
  /* 
    A:B => S
    IF there is a node with cat A, and there is a child with cat B THEN
    there is at least one child whose cat belongs to the set S
    (NB: B can be a member of S, in this case, the condition always holds)
  */
  
  // collect the categories from which we want at least a child
  int wanted_cats[size()];
  
  jiter<std::list<int> > citer(children());
  for (int i=0; citer; ++i, ++citer)
    {
      wanted_cats[i] = *citer;
    }

  // create the corresponding IntSet
  IntSet cs(wanted_cats, size());

  Tree& tree = node.tree();

  BoolVarArray ok(tree, node.sizeDR(), 0, 1); // for checking the requirements
  BoolVar at_least_one(tree, 0, 1); //the requirement is met

  int i=0;
  FORDR(node,other) {
    BoolVar has_req_cat(tree, 0, 1);
    dom(tree, other.cat(), cs, has_req_cat);
    rel(tree, other.is_succ_of(node), BOT_AND, has_req_cat, ok[i]);
    i++;
  }
  rel(tree, BOT_OR, ok, at_least_one);

  FORDR(node, other)
    {
      BoolVar  b(tree,0,1);
      rel(tree, other.is_succ_of(node), BOT_AND, other.has_cat(child()),b);

      BoolVar pertinent(tree, 0, 1);
      BoolVar pertsat  (tree, 0, 1);
      // pertinence
      rel(tree,node.has_cat(parent()),BOT_AND, b, pertinent);
      // pertinence and satisfaction
      rel(tree,pertinent,BOT_AND, at_least_one, pertsat);
      //strong semantics:
      if (strong)
        rel(tree, pertinnent, BOT_IMP, pertsat, 1);
      node.pert().push_back(pertinent);
      node.pert_and_sat().push_back(pertsat);
     }
 }

void Exclusion::post(Node& node, bool strong)
{
  /*
    A:B <=/=> C
    IF there is a node with cat A, and there are children with cat B and C respectively THEN
    B and C are mutually exclusive
  */
  Tree& tree = node.tree();

  FORDR(node, other1) {
    FORDR(node, other2) {
      if (other1.idx() == other2.idx())
          continue;
      BoolVar b(tree, 0, 1);
      BoolVarArgs ok(4);
      ok[0] = node.has_cat(parent());
      ok[1] = other1.is_succ_of(node);
      ok[2] = other2.is_succ_of(node);
      rel(tree,other1.has_cat(child1()), BOT_OR, other2.has_cat(child2()), b);
      ok[3] = b;

      BoolVar pertinent(tree, 0, 1);
      BoolVar pertsat  (tree, 0, 1);

      // pertinence
      rel(tree, BOT_AND, ok, pertinent);
      // satisfaction
      BoolVar sat(tree, 0, 1);
      BoolVar not_sat(tree, 0, 1);
      rel(tree, other1.has_cat(child1()), BOT_AND, other2.has_cat(child2()), not_sat);
      rel(tree, sat, IRT_NQ, not_sat);
      // pertinence and satisfaction
      rel(tree, pertinent, BOT_AND, sat, pertsat);
      if (strong)
        rel(tree, pertinent, BOT_IMP, pertsat, 1);
      node.pert().push_back(pertinent);
      node.pert_and_sat().push_back(pertsat);
    }
  }
}

void Adjacence::post(Node& node, bool strong)
{
  /*
    A:B adj C
    IF there is a node with cat A, and there are children with cat B and C respectively THEN
    the subtree from B and the subtree from C are adjacent
  */
  Tree& tree = node.tree();

  FORDR(node, other1) {
    FORDR(node, other2) {
      if (other1.idx() == other2.idx())
          continue;
      BoolVarArgs ok(5);
      ok[0] = node.has_cat(parent());
      ok[1] = other1.is_succ_of(node);
      ok[2] = other2.is_succ_of(node);
      ok[3] = other1.has_cat(child1());
      ok[4] = other2.has_cat(child2());

      BoolVar pertinent(tree, 0, 1);
      BoolVar pertsat  (tree, 0, 1);
      // pertinence
      rel(tree, BOT_AND, ok, pertinent);
      // satisfaction
      BoolVar sat(tree, 0, 1);
      // MAX SEEMS TO RAISE AN INCONSISTENT CONSTRAINT ?? (MAX = UPPER BOUND ?)
      //IntVarArray max_proj(tree, 2, 0, node.M()-1);
      //max(tree, other1.cols(), max_proj[0]);
      //max(tree, other2.cols(), max_proj[1]);
      BoolVarArray ou(tree,2,0,1);
      //rel(tree, max_proj[0], IRT_EQ, other2.col()-1, ou[0]);
      //rel(tree, max_proj[1], IRT_EQ, other1.col()-1, ou[1]);
      dom(tree, other1.cols(), SRT_SUP, other2.col() -1, ou[0]);
      dom(tree, other2.cols(), SRT_SUP, other1.col() -1, ou[1]);
      rel(tree, BOT_OR, ou, sat);
      // pertinence and satisfaction
      rel(tree, pertinent, BOT_AND, sat, pertsat);
      if (strong)
        rel(tree, pertinent, BOT_IMP, pertsat, 1);
      node.pert().push_back(pertinent);
      node.pert_and_sat().push_back(pertsat);
    }
  }
}

